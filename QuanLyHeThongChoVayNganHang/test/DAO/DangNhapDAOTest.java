/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import model.Taikhoan;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lephucdienhung
 */
public class DangNhapDAOTest {
    
    public DangNhapDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getUser method, of class DangNhapDAO.
     */
    @Test
    public void testGetUser() {
        System.out.println("getUser");
        DangNhapDAO instance = new DangNhapDAO();
        Taikhoan result = instance.getUser();
        assertEquals("String", result.getTendangnhap());
        assertEquals("12345", result.getMatkhau());
    }

    /**
     * Test of getTen method, of class DangNhapDAO.
     */
    @Test
    public void testGetTen() {
        System.out.println("getTen");
        String tdn = "dienhung";
        String mk = "12345";
        DangNhapDAO instance = new DangNhapDAO();
        String expResult = "Lê Phúc Diên Hưng";
        String result = instance.getTen(tdn, mk);
        assertEquals(expResult, result);
    }
    
}
