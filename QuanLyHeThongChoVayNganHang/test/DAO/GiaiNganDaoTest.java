/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.util.ArrayList;
import model.Giaingan;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lephucdienhung
 */
public class GiaiNganDaoTest {

    public GiaiNganDaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void run() {
        testThemGiaingan();
        testThemGiaingan1();
        testThemGiaingan2();
        testThemGiaingan3();
        testThemGiaingan4();
        testThemGiaingan5();
        testGetGiaingan();
        testGetGiaingan1();
        testSuaGiaingan();
        testSuaGiaingan1();
        testSuaGiaingan2();
        testSuaGiaingan3();
        testSuaGiaingan4();
        testSuaGiaingan5();
        testXoaGiaiNgan();
        testXoaGiaiNgan1();
        testGetDanhsach();
    }
//    @Test

    public void testThemGiaingan() {
        System.out.println("themGiaingan");
        Giaingan giaingan = new Giaingan(-1, "", "", 123, 123, "");
        GiaiNganDao instance = new GiaiNganDao();
        int expResult = -2;
        int result = instance.themGiaingan(giaingan);
        assertEquals(expResult, result);

    }

//    @Test
    public void testThemGiaingan1() {
        System.out.println("themGiaingan1");
        Giaingan giaingan = new Giaingan(111, "", "", -2, 0, "");
        GiaiNganDao instance = new GiaiNganDao();
        int expResult = -3;
        int result = instance.themGiaingan(giaingan);
        assertEquals(expResult, result);
    }

//    @Test
    public void testThemGiaingan2() {
        System.out.println("themGiaingan1");
        Giaingan giaingan = new Giaingan(111, "", "", 2, -2, "");
        GiaiNganDao instance = new GiaiNganDao();
        int expResult = -4;
        int result = instance.themGiaingan(giaingan);
        assertEquals(expResult, result);

    }

//    @Test
    public void testThemGiaingan3() {
        System.out.println("themGiaingan3");
        Giaingan giaingan = new Giaingan(112, "", "", 2, 2, "");
        GiaiNganDao instance = new GiaiNganDao();
        int expResult = -5;
        int result = instance.themGiaingan(giaingan);
        assertEquals(expResult, result);

    }

//    @Test
    public void testThemGiaingan4() {
        System.out.println("themGiaingan3");
        Giaingan giaingan = new Giaingan(111, "", "", 2, 2, "2020-06-14");
        GiaiNganDao instance = new GiaiNganDao();
        int expResult = 1;
        int result = instance.themGiaingan(giaingan);
        assertEquals(expResult, result);
    }

//    @Test
    public void testThemGiaingan5() {
        System.out.println("themGiaingan3");
        Giaingan giaingan = new Giaingan(111, "", "", 2, 2, "2020-06-14");
        GiaiNganDao instance = new GiaiNganDao();
        int expResult = -1;
        int result = instance.themGiaingan(giaingan);
        assertEquals(expResult, result);
    }

//    @Test
    public void testGetGiaingan() {
        System.out.println("getGiaingan");
        int MaHs = 112;
        GiaiNganDao instance = new GiaiNganDao();
        int expResult = 112;
        int result = instance.getGiaingan(MaHs);
        assertEquals(expResult, result);
    }

//    @Test
    public void testGetGiaingan1() {
        System.out.println("getGiaingan1");
        int MaHs = 0;
        GiaiNganDao instance = new GiaiNganDao();
        int expResult = -1;
        int result = instance.getGiaingan(MaHs);
        System.out.println("testGetGiaingan1" + result);
        assertEquals(expResult, result);
    }

//    @Test
    public void testSuaGiaingan() {
        System.out.println("suaGiaingan");
        Giaingan giaingan = new Giaingan(-1, "", "", 123, 123, "");
        GiaiNganDao instance = new GiaiNganDao();
        int expResult = -2;
        int result = instance.suaGiaingan(giaingan);
        assertEquals(expResult, result);
    }

//    @Test
    public void testSuaGiaingan1() {
        System.out.println("suaGiaingan1");
        Giaingan giaingan = new Giaingan(111, "", "", -1, 123, "");
        GiaiNganDao instance = new GiaiNganDao();
        int expResult = -3;
        int result = instance.suaGiaingan(giaingan);
        assertEquals(expResult, result);
    }

//    @Test
    public void testSuaGiaingan2() {
        System.out.println("suaGiaingan2");
        Giaingan giaingan = new Giaingan(111, "", "", 123, -123, "");
        GiaiNganDao instance = new GiaiNganDao();
        int expResult = -4;
        int result = instance.suaGiaingan(giaingan);
        assertEquals(expResult, result);
    }

//    @Test
    public void testSuaGiaingan3() {
        System.out.println("suaGiaingan3");
        Giaingan giaingan = new Giaingan(111, "", "", 123, 123, "");
        GiaiNganDao instance = new GiaiNganDao();
        int expResult = -5;
        int result = instance.suaGiaingan(giaingan);
        assertEquals(expResult, result);
    }

//    @Test
    public void testSuaGiaingan4() {
        System.out.println("suaGiaingan4");
        Giaingan giaingan = new Giaingan(111, "", "", 123, 123, "2020-06-15");
        GiaiNganDao instance = new GiaiNganDao();
        int expResult = 1;
        int result = instance.suaGiaingan(giaingan);
        assertEquals(expResult, result);
    }
//    @Test

    public void testSuaGiaingan5() {
        System.out.println("suaGiaingan4");
        Giaingan giaingan = new Giaingan(12, "", "", 123, 123, "2020-06-14");
        GiaiNganDao instance = new GiaiNganDao();
        int expResult = -1;
        int result = instance.suaGiaingan(giaingan);
        assertEquals(expResult, result);
    }

//    @Test
    public void testXoaGiaiNgan() {
        System.out.println("xoaGiaiNgan");
        int MaHS = 0;
        GiaiNganDao instance = new GiaiNganDao();
        boolean expResult = false;
        boolean result = instance.xoaGiaiNgan(MaHS);
        assertEquals(expResult, result);
    }

//    @Test
    public void testXoaGiaiNgan1() {
        System.out.println("xoaGiaiNgan1");
        int MaHS = 111;
        GiaiNganDao instance = new GiaiNganDao();
        boolean expResult = true;
        boolean result = instance.xoaGiaiNgan(MaHS);
        assertEquals(expResult, result);
    }

//    @Test
    public void testGetDanhsach() {
        System.out.println("getDanhsach");
        GiaiNganDao instance = new GiaiNganDao();
        ArrayList<Giaingan> expResult = new ArrayList<>();
        expResult.add(new Giaingan(112, "", "", 123, 123, "2020-06-15"));

        ArrayList<Giaingan> result = instance.getDanhsach();
        assertEquals(expResult.size(), result.size());
        if (expResult.size() == result.size()) {
            for (int i = 0; i < expResult.size(); i++) {
                Giaingan mot = expResult.get(i);
                Giaingan hai = result.get(i);
                assertEquals(mot.getGhichu(), hai.getGhichu());
                assertEquals(mot.getLoaichungtu(), hai.getLoaichungtu());
                assertEquals(mot.getMaHS(), hai.getMaHS());
                assertEquals(mot.getNgaygiaingan(), hai.getNgaygiaingan());
                assertEquals(mot.getSotienchuagiaingan(), hai.getSotienchuagiaingan());
                assertEquals(mot.getSotiengiaingan(), hai.getSotiengiaingan());

            }

        }

    }

}
