/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.util.ArrayList;
import model.BangThuKhachHang;
import model.ThongtinKhachhang;
import model.Tinhkhoanvay;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lephucdienhung
 */
public class TinhkhoanvayDaoTest {

    public TinhkhoanvayDaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of tinhDunogoc method, of class TinhkhoanvayDao.
     */
    @Test
    public void testTinhDunogoc() {
        System.out.println("tinhDunogoc");
        BangThuKhachHang bangThuKhachHang = new BangThuKhachHang(1234567, 7.4, 13);
        TinhkhoanvayDao instance = new TinhkhoanvayDao();
        ArrayList<Tinhkhoanvay> expResult = new ArrayList<>();
        expResult.add(new Tinhkhoanvay(1139600, 94966, 7027));
        expResult.add(new Tinhkhoanvay(1044633, 94966, 7027));
        expResult.add(new Tinhkhoanvay(949666, 94966, 7027));
        expResult.add(new Tinhkhoanvay(854700, 94966, 7027));
        expResult.add(new Tinhkhoanvay(759733, 94966, 7027));
        expResult.add(new Tinhkhoanvay(664766, 94966, 7027));
        expResult.add(new Tinhkhoanvay(569800, 94966, 7027));
        expResult.add(new Tinhkhoanvay(474833, 94966, 7027));
        expResult.add(new Tinhkhoanvay(379866, 94966, 7027));
        expResult.add(new Tinhkhoanvay(284900, 94966, 7027));
        expResult.add(new Tinhkhoanvay(189933, 94966, 7027));
        expResult.add(new Tinhkhoanvay(94966, 94966, 7027));
        expResult.add(new Tinhkhoanvay(0, 94966, 7027));
        ArrayList<Tinhkhoanvay> result = instance.tinhDunogoc(bangThuKhachHang);

        assertEquals(expResult.size(), result.size());
        if (expResult.size() == result.size()) {
            for (int i = 0; i < expResult.size(); i++) {
                Tinhkhoanvay mot = expResult.get(i);
                Tinhkhoanvay hai = result.get(i);
                assertEquals(String.valueOf(mot.getGoc()), String.valueOf(hai.getGoc()));
                assertEquals(String.valueOf(mot.getLai()), String.valueOf(hai.getLai()));
                assertEquals(String.valueOf(mot.getTiengocconlai()), String.valueOf(hai.getTiengocconlai()));

            }
        }

    }

    /**
     * Test of tinhDunogiamdan method, of class TinhkhoanvayDao.
     */
    @Test
    public void testTinhDunogiamdan() {
        System.out.println("tinhDunogiamdan");

        BangThuKhachHang bangThuKhachHang = new BangThuKhachHang(152000000, 8.5, 14);
        TinhkhoanvayDao instance = new TinhkhoanvayDao();
        ArrayList<Tinhkhoanvay> expResult = new ArrayList<>();
        expResult.add(new Tinhkhoanvay(141142857, 10857142, 922857));
        expResult.add(new Tinhkhoanvay(130285714, 10857142, 856938));
        expResult.add(new Tinhkhoanvay(119428571, 10857142, 791020));
        expResult.add(new Tinhkhoanvay(108571428, 10857142, 725102));
        expResult.add(new Tinhkhoanvay(97714285, 10857142, 659183));
        expResult.add(new Tinhkhoanvay(86857142, 10857142, 593265));
        expResult.add(new Tinhkhoanvay(76000000, 10857142, 527346));
        expResult.add(new Tinhkhoanvay(65142857, 10857142, 461428));
        expResult.add(new Tinhkhoanvay(54285714, 10857142, 395510));
        expResult.add(new Tinhkhoanvay(43428571, 10857142, 329591));
        expResult.add(new Tinhkhoanvay(32571428, 10857142, 263673));
        expResult.add(new Tinhkhoanvay(21714285, 10857142, 197755));
        expResult.add(new Tinhkhoanvay(10857142, 10857142, 131836));
        expResult.add(new Tinhkhoanvay(0, 10857142, 65918));
         ArrayList<Tinhkhoanvay> result = instance.tinhDunogiamdan(bangThuKhachHang);
        
        assertEquals(expResult.size(), result.size());
        if (expResult.size() == result.size()) {
            for (int i = 0; i < expResult.size(); i++) {
                Tinhkhoanvay mot = expResult.get(i);
                Tinhkhoanvay hai = result.get(i);
                assertEquals(String.valueOf(mot.getGoc()), String.valueOf(hai.getGoc()));
                assertEquals(String.valueOf(mot.getLai()), String.valueOf(hai.getLai()));
                assertEquals(String.valueOf(mot.getTiengocconlai()), String.valueOf(hai.getTiengocconlai()));

            }
        }
    }

}
