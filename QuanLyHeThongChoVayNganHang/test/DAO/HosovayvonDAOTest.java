/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.util.ArrayList;
import model.Hosovayvon;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lephucdienhung
 */
public class HosovayvonDAOTest {

    public HosovayvonDAOTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void run() {
        testXoaHoSoVayVon();
        testXoaHoSoVayVon1();
        testThemHosovayvon();
        testThemHosovayvon1();
        testThemHosovayvon2();
        testThemHosovayvon3();
        testThemHosovayvon4();
        testThemHosovayvon5();
        testThemHosovayvon6();
        testThemHosovayvon7();
        testThemHosovayvon8();
        testThemHosovayvon9();
        testGetHosovayvon();
        testGetHosovayvon1();
        testSuaHosovayvon();
        testSuaHosovayvon1();
        testSuaHosovayvon2();
        testSuaHosovayvon3();
        testSuaHosovayvon4();
        testSuaHosovayvon5();
        testSuaHosovayvon6();
        testSuaHosovayvon7();
        testSuaHosovayvon8();
        testSuaHosovayvon9();
        testGetDanhsach();
    }

//    @Test
    public void testThemHosovayvon() {
        System.out.println("themHosovayvon");
        Hosovayvon hosovayvon = new Hosovayvon(111, -2, 1, "", 3, "", 4, 5, 6, 7, 8, 9, "", "");
        HosovayvonDAO instance = new HosovayvonDAO();
        int expResult = -2;
        int result = instance.themHosovayvon(hosovayvon);
        assertEquals(expResult, result);
    }

//    @Test
    public void testThemHosovayvon1() {
        System.out.println("themHosovayvon1");
        Hosovayvon hosovayvon = new Hosovayvon(111, 111, 1, "", -3, "", 4, 5, 6, 7, 8, 9, "", "");
        HosovayvonDAO instance = new HosovayvonDAO();
        int expResult = -3;
        int result = instance.themHosovayvon(hosovayvon);
        assertEquals(expResult, result);

    }

//    @Test
    public void testThemHosovayvon2() {
        System.out.println("themHosovayvon2");
        Hosovayvon hosovayvon = new Hosovayvon(111, 111, 1, "", 3, "", -4, 5, 6, 7, 8, 9, "", "");
        HosovayvonDAO instance = new HosovayvonDAO();
        int expResult = -4;
        int result = instance.themHosovayvon(hosovayvon);
        assertEquals(expResult, result);

    }

//    @Test
    public void testThemHosovayvon3() {
        System.out.println("themHosovayvon3");
        Hosovayvon hosovayvon = new Hosovayvon(111, 111, 1, "", 3, "", 4, -5, 6, 7, 8, 9, "", "");
        HosovayvonDAO instance = new HosovayvonDAO();
        int expResult = -5;
        int result = instance.themHosovayvon(hosovayvon);
        assertEquals(expResult, result);

    }

//    @Test
    public void testThemHosovayvon4() {
        System.out.println("themHosovayvon4");
        Hosovayvon hosovayvon = new Hosovayvon(111, 111, 1, "", 3, "", 4, (float) 1.3, 6, 7, 8, 9, "", "");
        HosovayvonDAO instance = new HosovayvonDAO();
        int expResult = -6;
        int result = instance.themHosovayvon(hosovayvon);
        assertEquals(expResult, result);

    }

//    @Test
    public void testThemHosovayvon5() {
        System.out.println("themHosovayvon5");
        Hosovayvon hosovayvon = new Hosovayvon(111, 111, 1, "", 3, "", 4, (float) 1.3, 1, 7, 8, 9, "", "");
        HosovayvonDAO instance = new HosovayvonDAO();
        int expResult = -7;
        int result = instance.themHosovayvon(hosovayvon);
        assertEquals(expResult, result);

    }

//    @Test
    public void testThemHosovayvon6() {
        System.out.println("themHosovayvon6");
        Hosovayvon hosovayvon = new Hosovayvon(111, 111, 1, "", 3, "", 4, (float) 1.3, 0, 1, 8, 9, "", "");
        HosovayvonDAO instance = new HosovayvonDAO();
        int expResult = -8;
        int result = instance.themHosovayvon(hosovayvon);
        assertEquals(expResult, result);

    }

//    @Test
    public void testThemHosovayvon7() {
        System.out.println("themHosovayvon7");
        Hosovayvon hosovayvon = new Hosovayvon(111, 111, 1, "", 3, "", 4, (float) 1.3, 0, 1, 0, 9, "", "");
        HosovayvonDAO instance = new HosovayvonDAO();
        int expResult = -9;
        int result = instance.themHosovayvon(hosovayvon);
        assertEquals(expResult, result);

    }

//    @Test
    public void testThemHosovayvon8() {
        System.out.println("themHosovayvon8");
        Hosovayvon hosovayvon = new Hosovayvon(111, 111, 1, "113", 3, "1997-03-14", 4, (float) 1.3, 0, 1, 0, 1, "Không", "Không");
        HosovayvonDAO instance = new HosovayvonDAO();
        int expResult = 1;
        int result = instance.themHosovayvon(hosovayvon);
        assertEquals(expResult, result);
    }

//    @Test
    public void testThemHosovayvon9() {
        System.out.println("themHosovayvon9");
        Hosovayvon hosovayvon = new Hosovayvon(111, 111, 1, "", 3, "", 4, (float) 1.3, 0, 1, 0, 1, "", "");
        HosovayvonDAO instance = new HosovayvonDAO();
        int expResult = -1;
        int result = instance.themHosovayvon(hosovayvon);
        assertEquals(expResult, result);
    }

//    @Test
    public void testGetHosovayvon() {
        System.out.println("getHosovayvon");
        int MaHs = 0;
        HosovayvonDAO instance = new HosovayvonDAO();
        int expResult = -1;
        int result = instance.getHosovayvon(MaHs);
        assertEquals(expResult, result);
    }

//    @Test
    public void testGetHosovayvon1() {
        System.out.println("getHosovayvon1");
        int MaHs = 112;
        HosovayvonDAO instance = new HosovayvonDAO();
        int expResult = 112;
        int result = instance.getHosovayvon(MaHs);
        assertEquals(expResult, result);
    }

//    @Test
    public void testSuaHosovayvon() {
        System.out.println("suaHosovayvon");
        Hosovayvon hosovayvon = new Hosovayvon(111, -2, 1, "", 3, "", 4, 5, 6, 7, 8, 9, "", "");
        HosovayvonDAO instance = new HosovayvonDAO();
        int expResult = -2;
        int result = instance.suaHosovayvon(hosovayvon);
        assertEquals(expResult, result);
    }

//    @Test
    public void testSuaHosovayvon1() {
        System.out.println("suaHosovayvon1");
        Hosovayvon hosovayvon = new Hosovayvon(111, 111, 1, "", -3, "", 4, (float) 1.3, 0, 1, 0, 1, "", "");
        HosovayvonDAO instance = new HosovayvonDAO();
        int expResult = -3;
        int result = instance.suaHosovayvon(hosovayvon);
        assertEquals(expResult, result);
    }

//    @Test
    public void testSuaHosovayvon2() {
        System.out.println("suaHosovayvon2");
        Hosovayvon hosovayvon = new Hosovayvon(111, 111, 1, "", 3, "", -4, (float) 1.3, 0, 1, 0, 1, "", "");
        HosovayvonDAO instance = new HosovayvonDAO();
        int expResult = -4;
        int result = instance.suaHosovayvon(hosovayvon);
        assertEquals(expResult, result);
    }

//    @Test
    public void testSuaHosovayvon3() {
        System.out.println("suaHosovayvon3");
        Hosovayvon hosovayvon = new Hosovayvon(111, 111, 1, "", 3, "", 4, (float) -1.3, 0, 1, 0, 1, "", "");
        HosovayvonDAO instance = new HosovayvonDAO();
        int expResult = -5;
        int result = instance.suaHosovayvon(hosovayvon);
        assertEquals(expResult, result);
    }

//    @Test
    public void testSuaHosovayvon4() {
        System.out.println("suaHosovayvon4");
        Hosovayvon hosovayvon = new Hosovayvon(111, 111, 1, "", 3, "", 4, (float) 1.3, 9, 1, 0, 1, "", "");
        HosovayvonDAO instance = new HosovayvonDAO();
        int expResult = -6;
        int result = instance.suaHosovayvon(hosovayvon);
        assertEquals(expResult, result);
    }

//    @Test
    public void testSuaHosovayvon5() {
        System.out.println("suaHosovayvon5");
        Hosovayvon hosovayvon = new Hosovayvon(111, 111, 1, "", 3, "", 4, (float) 1.3, 0, 8, 0, 1, "", "");
        HosovayvonDAO instance = new HosovayvonDAO();
        int expResult = -7;
        int result = instance.suaHosovayvon(hosovayvon);
        assertEquals(expResult, result);
    }

//    @Test
    public void testSuaHosovayvon6() {
        System.out.println("suaHosovayvon6");
        Hosovayvon hosovayvon = new Hosovayvon(111, 111, 1, "", 3, "", 4, (float) 1.3, 0, 1, 7, 1, "", "");
        HosovayvonDAO instance = new HosovayvonDAO();
        int expResult = -8;
        int result = instance.suaHosovayvon(hosovayvon);
        assertEquals(expResult, result);
    }

//    @Test
    public void testSuaHosovayvon7() {
        System.out.println("suaHosovayvon7");
        Hosovayvon hosovayvon = new Hosovayvon(111, 111, 1, "", 3, "", 4, (float) 1.3, 0, 1, 0, 2, "", "");
        HosovayvonDAO instance = new HosovayvonDAO();
        int expResult = -9;
        int result = instance.suaHosovayvon(hosovayvon);
        assertEquals(expResult, result);
    }

//    @Test
    public void testSuaHosovayvon8() {
        System.out.println("suaHosovayvon8");
        Hosovayvon hosovayvon = new Hosovayvon(111, 111, 1, "113", 3, "2020-03-14", 4, (float) 1.3, 0, 1, 0, 1, "Lê Lâm", "Không");
        HosovayvonDAO instance = new HosovayvonDAO();
        int expResult = 1;
        int result = instance.suaHosovayvon(hosovayvon);
        assertEquals(expResult, result);
    }
//    @Test

    public void testSuaHosovayvon9() {
        System.out.println("suaHosovayvon8");
        Hosovayvon hosovayvon = new Hosovayvon(111, 11, 1, "113", 3, "2020-03-14", 4, (float) 1.3, 0, 1, 0, 1, "Không", "Không");
        HosovayvonDAO instance = new HosovayvonDAO();
        int expResult = -1;
        int result = instance.suaHosovayvon(hosovayvon);
        assertEquals(expResult, result);
    }

//    @Test
    public void testXoaHoSoVayVon() {
        System.out.println("xoaHoSoVayVon");
        int MaHS = 0;
        HosovayvonDAO instance = new HosovayvonDAO();
        boolean expResult = false;
        boolean result = instance.xoaHoSoVayVon(MaHS);
        assertEquals(expResult, result);

    }

//    @Test
    public void testXoaHoSoVayVon1() {
        System.out.println("xoaHoSoVayVon1");
        int MaHS = 111;
        HosovayvonDAO instance = new HosovayvonDAO();
        boolean expResult = true;
        boolean result = instance.xoaHoSoVayVon(MaHS);
        assertEquals(expResult, result);
    }

//    @Test
    public void testGetDanhsach() {
        System.out.println("getDanhsach");
        HosovayvonDAO instance = new HosovayvonDAO();
        ArrayList<Hosovayvon> expResult = new ArrayList<>();
        expResult.add(new Hosovayvon(111, 111, 1, "113", 3, "2020-03-14", 4, (float) 1.3, 0, 1, 0, 1, "Lê Lâm", "Không"));
        expResult.add(new Hosovayvon(111, 112, 1, "123", 3, "2020-01-04", 12, (float) 1.37, 0, 1, 1, 0, "Không", "Chưa có bìa hộ khẩu"));
        ArrayList<Hosovayvon> result = instance.getDanhsach();
        assertEquals(result.size(), expResult.size());
        if (expResult.size() == result.size()) {
            for (int i = 0; i < expResult.size(); i++) {
                Hosovayvon mot = expResult.get(i);
                Hosovayvon hai = result.get(i);
                assertEquals(mot.getGhichu(), hai.getGhichu());
                assertEquals(mot.getGocdinhky(), hai.getGocdinhky());
                assertEquals(mot.getHanvay(), hai.getHanvay());
                assertEquals(mot.getHinhthucvay(), hai.getHinhthucvay());
                assertEquals(String.valueOf(mot.getLaiSuat()), String.valueOf(hai.getLaiSuat()));
                assertEquals(mot.getLaidinhky(), hai.getLaidinhky());
                assertEquals(mot.getMahs(), hai.getMahs());
                assertEquals(mot.getMakh(), hai.getMakh());
                assertEquals(mot.getManv(), hai.getManv());
                assertEquals(mot.getMavung(), hai.getMavung());
                assertEquals(mot.getMucdichvay(), hai.getMucdichvay());
                assertEquals(mot.getNgaygiaingan(), hai.getNgaygiaingan());
                assertEquals(mot.getSotienvay(), hai.getSotienvay());
                assertEquals(mot.getThuake(), hai.getThuake());

            }

        }
    }
}
