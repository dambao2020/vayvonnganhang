/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.util.ArrayList;
import model.Giaingan;
import model.ThongtinKhachhang;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lephucdienhung
 */
public class ThongtinKhachhangDaoTest {

    public ThongtinKhachhangDaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void run() {
        testGetThongtinKhachhang();
        testGetThongtinKhachhang1();
        testXoaThongtinKhachhang();
        testXoaThongtinKhachhang1();
        testThemThongtinKhachhang();
        testThemThongtinKhachhang1();
        testThemThongtinKhachhang2();
        testSuaThongtinKhachhang();
        testSuaThongtinKhachhang1();
        testSuaThongtinKhachhang2();
        testGetDanhsach();
    }

//    @Test
    public void testGetThongtinKhachhang() {
        System.out.println("getThongtinKhachhang");
        int Makh = 0;
        ThongtinKhachhangDao instance = new ThongtinKhachhangDao();
        int expResult = -1;
        int result = instance.getThongtinKhachhang(Makh);
        assertEquals(expResult, result);
    }

//    @Test
    public void testGetThongtinKhachhang1() {
        System.out.println("getThongtinKhachhang1");
        int Makh = 110;
        ThongtinKhachhangDao instance = new ThongtinKhachhangDao();
        int expResult = 110;
        int result = instance.getThongtinKhachhang(Makh);
        assertEquals(expResult, result);

    }

//    @Test
    public void testXoaThongtinKhachhang() {
        System.out.println("xoaThongtinKhachhang");
        int Makh = 111;
        ThongtinKhachhangDao instance = new ThongtinKhachhangDao();
        boolean expResult = true;
        boolean result = instance.xoaThongtinKhachhang(Makh);
        assertEquals(expResult, result);
    }

//    @Test
    public void testXoaThongtinKhachhang1() {
        System.out.println("xoaThongtinKhachhang1");
        int Makh = 0;
        ThongtinKhachhangDao instance = new ThongtinKhachhangDao();
        boolean expResult = false;
        boolean result = instance.xoaThongtinKhachhang(Makh);
        assertEquals(expResult, result);
    }

//    @Test
    public void testThemThongtinKhachhang() {
        System.out.println("themThongtinKhachhang");
        ThongtinKhachhang thongtinKhachhang = new ThongtinKhachhang(-1, "", "", "", "", "", true, "", "", "");
        ThongtinKhachhangDao instance = new ThongtinKhachhangDao();
        int expResult = -2;
        int result = instance.themThongtinKhachhang(thongtinKhachhang);
        assertEquals(expResult, result);
    }

//    @Test
    public void testThemThongtinKhachhang1() {
        System.out.println("themThongtinKhachhang1");
        ThongtinKhachhang thongtinKhachhang = new ThongtinKhachhang(111, "Tùng Sơn", "0397434101", "Hà Nội", "1997-06-17",
                "2020-06-17", true, "Ca Thanh Hoá", "Mường", "Không");
        ThongtinKhachhangDao instance = new ThongtinKhachhangDao();
        int expResult = 1;
        int result = instance.themThongtinKhachhang(thongtinKhachhang);
        assertEquals(expResult, result);
    }

//    @Test
    public void testThemThongtinKhachhang2() {
        System.out.println("themThongtinKhachhang2");
        ThongtinKhachhang thongtinKhachhang = new ThongtinKhachhang(112, "Tùng Sơn", "0397434101", "Thanh Hoá", "1997-06-17", "2020-06-17",
                true, "Ca Thanh Hoá", "Mường", "Không");
        ThongtinKhachhangDao instance = new ThongtinKhachhangDao();
        int expResult = -1;
        int result = instance.themThongtinKhachhang(thongtinKhachhang);
        assertEquals(expResult, result);
    }

    /**
     * Test of suaThongtinKhachhang method, of class ThongtinKhachhangDao.
     */
//    @Test
    public void testSuaThongtinKhachhang() {
        System.out.println("suaThongtinKhachhang");
        ThongtinKhachhang thongtinKhachhang = new ThongtinKhachhang(-1, "Lê Phúc Diên Hưng", "0397434101", "Thanh Hoá", "1997-06-17", "2020-06-17", 
                true, "Ca Thanh Hoá", "Mường", "Không");
        ThongtinKhachhangDao instance = new ThongtinKhachhangDao();
        int expResult = -2;
        int result = instance.suaThongtinKhachhang(thongtinKhachhang);
        assertEquals(expResult, result);
    }

//    @Test
    public void testSuaThongtinKhachhang1() {
        System.out.println("suaThongtinKhachhang1");
        ThongtinKhachhang thongtinKhachhang = new ThongtinKhachhang(111, "Lê Phúc Diên Hưng", "0397434101", "Thanh Hoá", "1997-06-17", "2020-06-17", true, "Ca Thanh Hoá",
                "Mường", "Không");
        ThongtinKhachhangDao instance = new ThongtinKhachhangDao();
        int expResult = 1;
        int result = instance.suaThongtinKhachhang(thongtinKhachhang);
        assertEquals(expResult, result);
    }

//    @Test
    public void testSuaThongtinKhachhang2() {
        System.out.println("suaThongtinKhachhang2");
        ThongtinKhachhang thongtinKhachhang = new ThongtinKhachhang(1, "Lê Phúc Diên Hưng", "0397434101", "Thanh Hoá", "1997-06-17", "2020-06-17", true, "Ca Thanh Hoá",
                "Mường", "Không");
        ThongtinKhachhangDao instance = new ThongtinKhachhangDao();
        int expResult = -1;
        int result = instance.suaThongtinKhachhang(thongtinKhachhang);
        assertEquals(expResult, result);
    }

    /**
     * Test of getDanhsach method, of class ThongtinKhachhangDao.
     */
//    @Test
    public void testGetDanhsach() {
        System.out.println("getDanhsach");
        ThongtinKhachhangDao instance = new ThongtinKhachhangDao();
        ArrayList<ThongtinKhachhang> expResult = new ArrayList<>();
        expResult.add(new ThongtinKhachhang(110, "Lê Phúc Diên Hưng", "0397434101", "Thanh Hoá", "2020-06-17", "2020-06-17", true, "Ca Thanh Hoá", "Mường", ""));
        expResult.add(new ThongtinKhachhang(111, "Lê Phúc Diên Hưng", "0397434101", "Thanh Hoá", "1997-06-17", "2020-06-17", true, "Ca Thanh Hoá", "Mường", "Không"));
        expResult.add(new ThongtinKhachhang(112, "Lê Phúc Diên Hưng", "0397434101", "Thanh Hoá", "1997-06-17", "1997-06-17", true, "Ca Thanh Hoá", "Mường", ""));

        ArrayList<ThongtinKhachhang> result = instance.getDanhsach();
        assertEquals(expResult.size(), expResult.size());
        System.out.println(result.size());
        if (expResult.size() == result.size()) {
            System.out.println(result.size());
            System.out.println(result.size());
            for (int i = 0; i < expResult.size(); i++) {
                ThongtinKhachhang mot = expResult.get(i);
                ThongtinKhachhang hai = result.get(i);
                System.out.println(mot.getMakh());
                assertEquals(mot.getGhichu(), hai.getGhichu());
                assertEquals(mot.getCmnd(), hai.getCmnd());
                assertEquals(mot.getDantoc(), hai.getDantoc());
                assertEquals(mot.getDiachi(), hai.getDiachi());
                assertEquals(mot.getMakh(), hai.getMakh());
                assertEquals(mot.getNgaycap(), hai.getNgaycap());
                assertEquals(mot.getNgaysinh(), hai.getNgaysinh());
                assertEquals(mot.getNoicap(), hai.getNoicap());
                assertEquals(mot.getTenkh(), hai.getTenkh());

            }

        }
    }

}
