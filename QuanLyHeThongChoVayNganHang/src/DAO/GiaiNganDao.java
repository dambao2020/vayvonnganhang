/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import database.clsConnectDB;
import model.Giaingan;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import static view.GiaiNganJPanel.ThongBao;
import view.ThongTinKHJPanel;

/**
 *
 * @author lephucdienhung
 */
public class GiaiNganDao {

    clsConnectDB connection = new database.clsConnectDB();

    public int getGiaingan(int MaHs) {
        Giaingan c = new Giaingan();
        String cautruyvan = "SELECT * FROM GiaiNgan WHERE MaHoSo ='" + MaHs + "'";
        ResultSet rs = connection.ExcuteQueryGetTable(cautruyvan);
        try {
            while (rs.next()) {
                c.setMaHS(rs.getInt("MaHoSo"));
                c.setSotiengiaingan(rs.getInt("TienGiaiNgan"));
                c.setSotienchuagiaingan(rs.getInt("TienChuaGiaiNgan"));
                c.setNgaygiaingan(rs.getString("NgayGiaiNgan"));
                c.setLoaichungtu(rs.getString("LoaiChungTu"));
                if (rs.getString("GhiChu") == null) {
                    c.setGhichu("Không có dữ liệu");
                } else {
                    c.setGhichu(rs.getString("GhiChu"));
                }
                return c.getMaHS();
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return -1;
    }

    public boolean xoaGiaiNgan(int MaHS) {
        String cautruyvan = "delete GiaiNgan where MaHoSo=" + MaHS;
        if (getGiaingan(MaHS) == MaHS) {
            connection.ExcuteQueryUpdateDB(cautruyvan);
            if (getGiaingan(MaHS) == -1) {
                return true;
            }
        }
        return false;
    }

    public int themGiaingan(Giaingan giaingan) {
        int MaHs = giaingan.getMaHS();
        int SoTienGiaiNgan = giaingan.getSotiengiaingan();
        int SoTienChuaGiaiNgan = giaingan.getSotienchuagiaingan();
        String ngaygiaingan = giaingan.getNgaygiaingan();
        if (MaHs <= 0) {
            return -2;
        } else if (SoTienGiaiNgan < 0) {
            return -3;
        } else if (SoTienChuaGiaiNgan < 0) {
            return -4;
        } else if (ngaygiaingan.equals("")) {
            return -5;
        } else {
            if (getGiaingan(MaHs) == -1) {
                String LoaiChungTu, GhiChu;
                LoaiChungTu = giaingan.getLoaichungtu();
                GhiChu = giaingan.getGhichu();
                String cautruyvan = "INSERT INTO GiaiNgan( MaHoSo,  TienGiaiNgan, TienChuaGiaiNgan, NgayGiaiNgan, LoaiChungTu, GhiChu) VALUES('"
                        + MaHs + "', '" + SoTienGiaiNgan + "' ,'" + SoTienChuaGiaiNgan + "','" + ngaygiaingan + "',N'"
                        + LoaiChungTu + "',N'" + GhiChu + "' )";
//            System.out.println(cautruyvan);

                connection.ExcuteQueryUpdateDB(cautruyvan);
                return 1;
            }
        }
        return -1;
    }

    public int suaGiaingan(Giaingan giaingan) {
        int MaHs = giaingan.getMaHS();
        int SoTienGiaiNgan = giaingan.getSotiengiaingan();
        int SoTienChuaGiaiNgan = giaingan.getSotienchuagiaingan();
        String ngaygiaingan = giaingan.getNgaygiaingan();
        if (MaHs <= 0) {
            return -2;
        } else if (SoTienGiaiNgan < 0) {
            return -3;
        } else if (SoTienChuaGiaiNgan < 0) {
            return -4;
        } else if (ngaygiaingan.equals("")) {
            return -5;
        } else {
            if (getGiaingan(MaHs) == MaHs) {
                String LoaiChungTu, GhiChu;
                LoaiChungTu = giaingan.getLoaichungtu();
                GhiChu = giaingan.getGhichu();
                String cautruyvan = "update  GiaiNgan set TienGiaiNgan=" + SoTienGiaiNgan + ",TienChuaGiaiNgan="
                        + SoTienChuaGiaiNgan + ",NgayGiaiNgan='" + ngaygiaingan + "',LoaiChungTu=N'" + LoaiChungTu
                        + "',GhiChu=N'" + GhiChu + "' where MaHoSo=" + MaHs;
//                System.out.println(cautruyvan);
                connection.ExcuteQueryUpdateDB(cautruyvan);
                return 1;
            }
        }
        return -1;
    }

    public ArrayList<Giaingan> getDanhsach() {
        ArrayList<Giaingan> dsGiaingan = new ArrayList<>();
        String cautruyvan = "";
        // System.out.println("vào được đây rồi");
        cautruyvan = "SELECT * FROM GiaiNgan";
        ResultSet rs = connection.ExcuteQueryGetTable(cautruyvan);
        try {
            while (rs.next()) {
                Giaingan c = new Giaingan();
                c.setMaHS(rs.getInt("MaHoSo"));
                c.setSotiengiaingan(rs.getInt("TienGiaiNgan"));
                c.setSotienchuagiaingan(rs.getInt("TienChuaGiaiNgan"));
                c.setNgaygiaingan(rs.getString("NgayGiaiNgan"));
                System.out.println(rs.getDate("NgayGiaiNgan"));
                c.setLoaichungtu(rs.getString("LoaiChungTu"));
                if (rs.getString("GhiChu") == null) {
                    c.setGhichu("Không có dữ liệu");
                } else {
                    c.setGhichu(rs.getString("GhiChu"));
                }

                dsGiaingan.add(c);
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return dsGiaingan;
    }
}
