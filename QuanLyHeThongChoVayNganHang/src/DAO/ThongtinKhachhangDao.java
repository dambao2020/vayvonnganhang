/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import database.clsConnectDB;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.ThongtinKhachhang;
import static view.ThongTinKHJPanel.ThongBao;

/**
 *
 * @author lephucdienhung
 */
public class ThongtinKhachhangDao {

    clsConnectDB connection = new database.clsConnectDB();

    public int getThongtinKhachhang(int Makh) {
        ThongtinKhachhang c = new ThongtinKhachhang();
        String cautruyvan = "SELECT * FROM KhachHang WHERE MaKhachHang ='" + Makh + "'";
        ResultSet rs = connection.ExcuteQueryGetTable(cautruyvan);
//        System.out.println(cautruyvan);
        try {
            while (rs.next()) {
                c.setMakh(rs.getInt("MaKhachHang"));
                c.setTenkh(rs.getString("TenKhachHang"));
                c.setNgaysinh(rs.getString("NgaySinh"));
                c.setDiachi(rs.getString("DiaChi"));
                int gioitinh = rs.getInt("GioiTinh");
                if (gioitinh == 1) {
                    c.setGioitinh(true);
                } else {
                    c.setGioitinh(false);
                }
                c.setCmnd(rs.getString("CMND"));
                c.setNgaycap(rs.getString("NgayCap"));
                c.setNgaycap(rs.getString("NoiCap"));
                c.setDantoc(rs.getString("DanToc"));

                if (rs.getString("GhiChu") == null) {
                    c.setGhichu("Không có dữ liệu");
                } else {
                    c.setGhichu(rs.getString("GhiChu"));
                }
                return c.getMakh();
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return -1;
    }

    public boolean xoaThongtinKhachhang(int Makh) {
        String cautruyvan = "delete KhachHang WHERE MaKhachHang ='" + Makh + "'";
        if (getThongtinKhachhang(Makh) == Makh) {
//            System.out.println(cautruyvan);
            connection.ExcuteQueryUpdateDB(cautruyvan);
            return true;
        } 
        return false;
    }

    public int themThongtinKhachhang(ThongtinKhachhang thongtinKhachhang) {
        int Makh = thongtinKhachhang.getMakh();

        if (Makh <= 0) {
            return -2;
        } else {
            if (getThongtinKhachhang(Makh) == -1) {
            String Cmnd, Tenkh, Diachi, Ghichu, Noicap, Dantoc;
            Cmnd = thongtinKhachhang.getCmnd();
            Tenkh = thongtinKhachhang.getTenkh();
            Ghichu = thongtinKhachhang.getGhichu();
            Noicap = thongtinKhachhang.getNoicap();
            Dantoc = thongtinKhachhang.getDantoc();
            Diachi = thongtinKhachhang.getDiachi();

            String Ngaysinh = thongtinKhachhang.getNgaysinh();
            String Ngaycap = thongtinKhachhang.getNgaycap();
            boolean Gioitinh = thongtinKhachhang.isGioitinh();
            int sex = 0;
            if (Gioitinh) {
                sex = 1;
            } else {
                sex = 0;
            }
            String cautruyvan = "INSERT INTO KhachHang( MaKhachHang,  TenKhachHang, NgaySinh, DiaChi, GioiTinh,"
                    + " CMND, NgayCap, NoiCap , DanToc, GhiChu) VALUES('"
                    + Makh + "', N'" + Tenkh + "' ,'" + Ngaysinh + "', N'" + Diachi + "'," + sex
                    + ",'" + Cmnd + "' , '" + Ngaycap + "', N'" + Noicap + "', N'" + Dantoc + "',N'" + Ghichu + "' )";
//            System.out.println(cautruyvan);
            
                connection.ExcuteQueryUpdateDB(cautruyvan);
                return 1;
            } 
        }
        return -1;
    }

    public int suaThongtinKhachhang(ThongtinKhachhang thongtinKhachhang) {
        int Makh = thongtinKhachhang.getMakh();

        if (Makh <= 0) {
            return -2;
        } else {
            if (getThongtinKhachhang(Makh) == Makh) {
                String Cmnd, Tenkh, Diachi, Ghichu, Noicap, Dantoc;
                Cmnd = thongtinKhachhang.getCmnd();
                Tenkh = thongtinKhachhang.getTenkh();
                Ghichu = thongtinKhachhang.getGhichu();
                Noicap = thongtinKhachhang.getNoicap();
                Dantoc = thongtinKhachhang.getDantoc();
                Diachi = thongtinKhachhang.getDiachi();

                String Ngaysinh = thongtinKhachhang.getNgaysinh();
                String Ngaycap = thongtinKhachhang.getNgaycap();
                boolean Gioitinh = thongtinKhachhang.isGioitinh();
                int sex = 0;
                if (Gioitinh) {
                    sex = 1;
                } else {
                    sex = 0;
                }

                String cautruyvan = "update  KhachHang set TenKhachHang=N'" + Tenkh
                        + "',NgaySinh='" + Ngaysinh + "',DiaChi=N'" + Diachi + "',GioiTinh=" + sex
                        + ",CMND='" + Cmnd + "',NgayCap='" + Ngaycap + "',NoiCap=N'" + Noicap
                        + "',DanToc=N'" + Dantoc + "',GhiChu=N'" + Ghichu + "' where MaKhachHang="
                        + Makh;
//                System.out.println(cautruyvan);
                connection.ExcuteQueryUpdateDB(cautruyvan);
                return 1;
            }
        }
        return -1;
    }

    public ArrayList<ThongtinKhachhang> getDanhsach() {
        ArrayList<ThongtinKhachhang> dsThongtinKhachhangs = new ArrayList<>();
        String cautruyvan = "SELECT * FROM KhachHang";
        ResultSet rs = connection.ExcuteQueryGetTable(cautruyvan);
        try {
            while (rs.next()) {
                ThongtinKhachhang c = new ThongtinKhachhang();
                c.setMakh(rs.getInt("MaKhachHang"));
                c.setTenkh(rs.getString("TenKhachHang"));
                c.setNgaysinh(rs.getString("NgaySinh"));
                c.setDiachi(rs.getString("DiaChi"));
                int gioitinh = rs.getInt("GioiTinh");
                if (gioitinh == 1) {
                    c.setGioitinh(true);
                } else {
                    c.setGioitinh(false);
                }
                c.setCmnd(rs.getString("CMND"));
                c.setNgaycap(rs.getString("NgayCap"));
                c.setNoicap(rs.getString("NoiCap"));
                c.setDantoc(rs.getString("DanToc"));

                if (rs.getString("GhiChu") == null) {
                    c.setGhichu("Không có dữ liệu");
                } else {
                    c.setGhichu(rs.getString("GhiChu"));
                }

                dsThongtinKhachhangs.add(c);
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return dsThongtinKhachhangs;
    }
}
