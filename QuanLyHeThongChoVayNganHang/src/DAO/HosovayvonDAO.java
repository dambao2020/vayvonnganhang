/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import database.clsConnectDB;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.Hosovayvon;
import static view.FrmDangNhap.ThongBao;

/**
 *
 * @author lephucdienhung
 */
public class HosovayvonDAO {

    clsConnectDB connection = new database.clsConnectDB();

    public int getHosovayvon(int MaHs) {
        Hosovayvon c = new Hosovayvon();
        String cautruyvan = "SELECT * FROM HoSoVayVon WHERE MaHoSo ='" + MaHs + "'";
        ResultSet rs = connection.ExcuteQueryGetTable(cautruyvan);
//        System.out.println(cautruyvan);
        try {
            while (rs.next()) {
                c.setMahs(rs.getInt("MaHoSo"));
                c.setHinhthucvay(rs.getInt("HinhThucVay"));
                c.setMahs(rs.getInt("MaHoSo"));
                c.setManv(rs.getInt("MaNhanVien"));
                c.setMakh(rs.getInt("MaKhachHang"));
                c.setSotienvay(rs.getInt("Tienvay"));
                c.setHanvay(rs.getInt("HanVay"));
                c.setNgaygiaingan(rs.getString("NgayVay"));
                c.setLaiSuat(rs.getFloat("LaiSuat"));
                c.setMucdichvay(rs.getInt("MucDich"));
                c.setMavung(rs.getString("MaVung"));
                c.setThuake(rs.getString("ThuaKe"));
                c.setGocdinhky(rs.getInt("GocDinhKy"));
                c.setLaidinhky(rs.getInt("LaiDinhKy"));
                if (rs.getString("GhiChu") == null) {
                    c.setGhichu("Không có dữ liệu");
                } else {
                    c.setGhichu(rs.getString("GhiChu"));
                }
                return c.getMahs();
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return -1;
    }

    public boolean xoaHoSoVayVon(int MaHS) {
        String cautruyvan = "delete HoSoVayVon WHERE MaHoSo ='" + MaHS + "'";
        if (getHosovayvon(MaHS) == MaHS) {
//            System.out.println(cautruyvan);
            connection.ExcuteQueryUpdateDB(cautruyvan);
            return true;
        } else {
            return false;
        }
    }

    public int themHosovayvon(Hosovayvon hosovayvon) {
        int Makh = hosovayvon.getMakh();
        int Mahs = hosovayvon.getMahs();
        int Manv = hosovayvon.getManv();
        int Sotienvay = hosovayvon.getSotienvay();
        int Hanvay = hosovayvon.getHanvay();
        int Mucdichvay = hosovayvon.getMucdichvay();
        int Hinhthucvay = hosovayvon.getHinhthucvay();
        int Laidinhky = hosovayvon.getLaidinhky();
        int Gocdinhky = hosovayvon.getGocdinhky();
        float Laisuat = hosovayvon.getLaiSuat();
        if (Mahs <= 0) {
            return -2;
        } else if (Sotienvay < 0) {
            return -3;
        } else if (Hanvay < 0) {
            return -4;
        } else if (Laisuat < 0) {
            return -5;
        } else if (Mucdichvay != 0 && Mucdichvay != 1) {
            return -6;
        } else if (Hinhthucvay != 0 && Hinhthucvay != 1) {
            return -7;
        } else if (Laidinhky != 0 && Laidinhky != 1) {
            return -8;
        } else if (Gocdinhky != 0 && Gocdinhky != 1) {
            return -9;
        } else {
            if (getHosovayvon(Mahs) == -1) {
                String Mavung, Thuake, GhiChu;
                Mavung = hosovayvon.getMavung();
                GhiChu = hosovayvon.getGhichu();
                Thuake = hosovayvon.getThuake();
                String Ngaygiaingan = hosovayvon.getNgaygiaingan();

                String cautruyvan = "INSERT INTO HoSoVayVon( MaKhachHang , MaHoSo , MaNhanVien , HinhThucVay , Tienvay , HanVay , NgayVay , "
                        + "LaiSuat , MucDich , MaVung , ThuaKe , GocDinhKy , LaiDinhKy, GhiChu) "
                        + "VALUES('"
                        + Makh + "', '" + Mahs + "' ,'" + Manv + "','" + Hinhthucvay + "', '" + Sotienvay + "' ,'" + Hanvay + "' ,'"
                        + Ngaygiaingan + "','" + Laisuat + "','" + Mucdichvay + "',N'" + Mavung + "' ,N'" + Thuake + "','" + Gocdinhky
                        + "','" + Laidinhky + "',N'" + GhiChu + "')";
//                System.out.println(cautruyvan);

                connection.ExcuteQueryUpdateDB(cautruyvan);
                return 1;
            } 
        }
        return -1;
    }

    public int suaHosovayvon(Hosovayvon hosovayvon) {
        int Makh = hosovayvon.getMakh();
        int Mahs = hosovayvon.getMahs();
        int Manv = hosovayvon.getManv();
        int Sotienvay = hosovayvon.getSotienvay();
        int Hanvay = hosovayvon.getHanvay();
        int Mucdichvay = hosovayvon.getMucdichvay();
        int Hinhthucvay = hosovayvon.getHinhthucvay();
        int Laidinhky = hosovayvon.getLaidinhky();
        int Gocdinhky = hosovayvon.getGocdinhky();
        float Laisuat = hosovayvon.getLaiSuat();
        if (Mahs <= 0) {
            return -2;
        } else if (Sotienvay < 0) {
            return -3;
        } else if (Hanvay < 0) {
            return -4;
        } else if (Laisuat < 0) {
            return -5;
        } else if (Mucdichvay != 0 && Mucdichvay != 1) {
            return -6;
        } else if (Hinhthucvay != 0 && Hinhthucvay != 1) {
            return -7;
        } else if (Laidinhky != 0 && Laidinhky != 1) {
            return -8;
        } else if (Gocdinhky != 0 && Gocdinhky != 1) {
            return -9;
        } else {
            if (getHosovayvon(Mahs) == Mahs) {
                String Mavung, Thuake, GhiChu;
                Mavung = hosovayvon.getMavung();
                GhiChu = hosovayvon.getGhichu();
                Thuake = hosovayvon.getThuake();
                String Ngaygiaingan = hosovayvon.getNgaygiaingan();

                String cautruyvan = "update  HoSoVayVon set MaKhachHang='" + Makh + "',MaNhanVien='" + Manv
                        + "',HinhThucVay='" + Hinhthucvay + "',Tienvay='" + Sotienvay + "',HanVay='" + Hanvay 
                        + "',NgayVay='"+ Ngaygiaingan + "',LaiSuat='" + Laisuat + "',MucDich='" + Mucdichvay + "',MaVung=N'" 
                        + Mavung + "',GhiChu=N'" + GhiChu+ "',GocDinhKy='" + Gocdinhky  + "',LaiDinhKy='" + Laidinhky+"',ThuaKe='" + Thuake
                        + "' where MaHoSo=" + Mahs;
//                System.out.println(cautruyvan);
                connection.ExcuteQueryUpdateDB(cautruyvan);
                return 1;
            }
        }
        return -1;
    }

    public ArrayList<Hosovayvon> getDanhsach() {
        ArrayList<Hosovayvon> dsHosovayvon = new ArrayList<>();
        String cautruyvan = "SELECT * FROM HoSoVayVon";
        ResultSet rs = connection.ExcuteQueryGetTable(cautruyvan);
        try {
            while (rs.next()) {
                Hosovayvon c = new Hosovayvon();
                c.setMahs(rs.getInt("MaHoSo"));
                c.setHinhthucvay(rs.getInt("HinhThucVay"));
                c.setMahs(rs.getInt("MaHoSo"));
                c.setManv(rs.getInt("MaNhanVien"));
                c.setMakh(rs.getInt("MaKhachHang"));
                c.setSotienvay(rs.getInt("Tienvay"));
                c.setHanvay(rs.getInt("HanVay"));
                c.setNgaygiaingan(rs.getString("NgayVay"));
                c.setLaiSuat(rs.getFloat("LaiSuat"));
                c.setMucdichvay(rs.getInt("MucDich"));
                c.setMavung(rs.getString("MaVung"));
                c.setThuake(rs.getString("ThuaKe"));
                c.setGocdinhky(rs.getInt("GocDinhKy"));
                c.setLaidinhky(rs.getInt("LaiDinhKy"));
                if (rs.getString("GhiChu") == null) {
                    c.setGhichu("Không có dữ liệu");
                } else {
                    c.setGhichu(rs.getString("GhiChu"));
                }

                dsHosovayvon.add(c);
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return dsHosovayvon;
    }
}
