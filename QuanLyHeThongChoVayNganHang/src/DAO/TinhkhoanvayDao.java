/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.util.ArrayList;
import model.BangThuKhachHang;
import model.Tinhkhoanvay;

/**
 *
 * @author lephucdienhung
 */
public class TinhkhoanvayDao {

    public ArrayList<Tinhkhoanvay> tinhDunogoc(BangThuKhachHang bangThuKhachHang) {
        ArrayList<Tinhkhoanvay> tinhkhoanvays = new ArrayList<>();
        double tienGoc = bangThuKhachHang.getSoTienVay() / bangThuKhachHang.getThoiHan();
        double tienGocConLai = bangThuKhachHang.getSoTienVay() - tienGoc;
        double laiSuatThang = bangThuKhachHang.getLaiSuat() / (bangThuKhachHang.getThoiHan() * 100);
        for (int j = 0; j < bangThuKhachHang.getThoiHan(); j++) {
            tinhkhoanvays.add(new Tinhkhoanvay((int) tienGocConLai, (int) tienGoc, (int) (bangThuKhachHang.getSoTienVay() * laiSuatThang)));
            tienGocConLai = tienGocConLai - tienGoc;
        }
        return tinhkhoanvays;
    }

    public ArrayList<Tinhkhoanvay> tinhDunogiamdan(BangThuKhachHang bangThuKhachHang) {
        ArrayList<Tinhkhoanvay> tinhkhoanvays = new ArrayList<>();
        double tienGoc = bangThuKhachHang.getSoTienVay() / bangThuKhachHang.getThoiHan();
        double tienGocConLai = bangThuKhachHang.getSoTienVay() - tienGoc;
        double laiSuatThang = bangThuKhachHang.getLaiSuat() / (bangThuKhachHang.getThoiHan() * 100);
        double soTienVay = bangThuKhachHang.getSoTienVay();
        for (int j = 0; j < bangThuKhachHang.getThoiHan(); j++) {
            tinhkhoanvays.add(new Tinhkhoanvay((int) tienGocConLai, (int) tienGoc, (int) (soTienVay * laiSuatThang)));
            soTienVay = tienGocConLai;
            tienGocConLai = tienGocConLai - tienGoc;
        }
        return tinhkhoanvays;
    }
}
