/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.awt.BorderLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JPanel;
import model.DanhMuc;
import view.GiaiNganJPanel;
import view.HoSoVayVonJPanel;
import view.ThongTinKHJPanel;
import view.TinhKhoanVayJPanel;

/**
 *
 * @author tungd
 */
public class ChuyenManHinhController {
    private JPanel view;
    private String kindSelected="";
    public ChuyenManHinhController(JPanel view) {
        this.view = view;
    }
    public void setView(JPanel ipnItem,JLabel jlbItem){
        kindSelected="ThongTinKhachHang";
        view.removeAll();
        view.setLayout(new BorderLayout());
        view.add(new ThongTinKHJPanel());
        view.validate();
        view.repaint();
    }
    public void setEvent(ArrayList<DanhMuc> arrayListItem){
        for (DanhMuc item : arrayListItem) {
            item.getJlb().addMouseListener(new LabelEvent(item.getKind(), item.getJpn(), item.getJlb()));
        }
    }
    public class LabelEvent implements MouseListener {
    private JPanel jPanel;
    private String kindSelected;
    private JPanel ipnItem;
    private JLabel jlbItem; 

    public LabelEvent(String kind, JPanel ipnItem, JLabel jlbItem) {
        this.kindSelected = kind;
        this.ipnItem = ipnItem;
        this.jlbItem = jlbItem;
    }
    @Override
    public void mouseClicked(MouseEvent e) {
         switch(kindSelected){
            case "ThongTinKH":{
                jPanel=new ThongTinKHJPanel();
                break;
            }
            case "HoSoVayVon":{
                jPanel=new HoSoVayVonJPanel();
                break;
            }
            case "GiaiNgan":{
                jPanel=new GiaiNganJPanel();
                break;
            }
            case "TinhKhoanVay":{
                jPanel=new TinhKhoanVayJPanel();
                break;
            }
            default:{
                break;
            }              
         }    
         view.removeAll();
         view.setLayout(new BorderLayout());
         view.add(jPanel);
         view.validate();
         view.repaint();
                 
    }

    @Override
    public void mousePressed(MouseEvent e) {
       
    }

    @Override
    public void mouseReleased(MouseEvent e) {
       
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        
    }

    @Override
    public void mouseExited(MouseEvent e) {
         
    }
    
}

    
    
    
}
