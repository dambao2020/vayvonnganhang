package model;

public class ThongtinKhachhang {

    private int Makh;
    private String Tenkh;
    private String Cmnd;
    private String Diachi;
    private String Ngaysinh;
    private String Ngaycap;
    private boolean Gioitinh;
    private String Noicap;
    private String Dantoc;
    private String Ghichu;

    public ThongtinKhachhang() {
        this.Makh = 0;
        this.Tenkh = "";
        this.Cmnd = "";
        this.Diachi = "";
        this.Ngaysinh = "";
        this.Ngaycap = "";
        this.Gioitinh = true;
        this.Noicap = "";
        this.Dantoc = "";
        this.Ghichu = "";
    }

    public ThongtinKhachhang(int Makh, String Tenkh, String Cmnd, String Diachi, String Ngaysinh, String Ngaycap,
            boolean Gioitinh, String Noicap, String Dantoc, String Ghichu) {
        this.Makh = Makh;
        this.Tenkh = Tenkh;
        this.Cmnd = Cmnd;
        this.Diachi = Diachi;
        this.Ngaysinh = Ngaysinh;
        this.Ngaycap = Ngaycap;
        this.Gioitinh = Gioitinh;
        this.Noicap = Noicap;
        this.Dantoc = Dantoc;
        this.Ghichu = Ghichu;
    }

    public int getMakh() {
        return Makh;
    }

    public void setMakh(int Makh) {
        this.Makh = Makh;
    }

    public String getTenkh() {
        return Tenkh;
    }

    public void setTenkh(String Tenkh) {
        this.Tenkh = Tenkh;
    }

    public String getCmnd() {
        return Cmnd;
    }

    public void setCmnd(String Cmnd) {
        this.Cmnd = Cmnd;
    }

    public String getDiachi() {
        return Diachi;
    }

    public void setDiachi(String Diachi) {
        this.Diachi = Diachi;
    }

    public String getNgaysinh() {
        return Ngaysinh;
    }

    public void setNgaysinh(String Ngaysinh) {
        this.Ngaysinh = Ngaysinh;
    }

    public String getNgaycap() {
        return Ngaycap;
    }

    public void setNgaycap(String Ngaycap) {
        this.Ngaycap = Ngaycap;
    }

    public boolean isGioitinh() {
        return Gioitinh;
    }

    public void setGioitinh(boolean Gioitinh) {
        this.Gioitinh = Gioitinh;
    }

    public String getNoicap() {
        return Noicap;
    }

    public void setNoicap(String Noicap) {
        this.Noicap = Noicap;
    }

    public String getDantoc() {
        return Dantoc;
    }

    public void setDantoc(String Dantoc) {
        this.Dantoc = Dantoc;
    }

    public String getGhichu() {
        return Ghichu;
    }

    public void setGhichu(String Ghichu) {
        this.Ghichu = Ghichu;
    }

    
    
    
}
