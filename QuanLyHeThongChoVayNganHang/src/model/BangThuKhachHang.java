/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author lephucdienhung
 */
public class BangThuKhachHang {

    private double SoTienVay;
    private double LaiSuat;
    private int ThoiHan;

    public BangThuKhachHang(double SoTienVay, double LaiSuat, int ThoiHan) {
        this.SoTienVay = SoTienVay;
        this.LaiSuat = LaiSuat;
        this.ThoiHan = ThoiHan;
    }

    public BangThuKhachHang() {
    }
    
    


    public double getSoTienVay() {
        return SoTienVay;
    }

    public void setSoTienVay(double SoTienVay) {
        this.SoTienVay = SoTienVay;
    }

    public double getLaiSuat() {
        return LaiSuat;
    }

    public void setLaiSuat(double LaiSuat) {
        this.LaiSuat = LaiSuat;
    }

    public int getThoiHan() {
        return ThoiHan;
    }

    public void setThoiHan(int ThoiHan) {
        this.ThoiHan = ThoiHan;
    }
}