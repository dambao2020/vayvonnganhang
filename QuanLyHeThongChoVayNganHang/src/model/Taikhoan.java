/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author lephucdienhung
 */
public class Taikhoan {

    private static String ten = "";
    private static String tendangnhap = "";
    private static String matkhau = "";

    public Taikhoan() {
    }
    

    public static String getTen() {
        return ten;
    }

    public static void setTen(String ten) {
        Taikhoan.ten = ten;
    }

    public static String getTendangnhap() {
        return tendangnhap;
    }

    public static void setTendangnhap(String tendangnhap) {
        Taikhoan.tendangnhap = tendangnhap;
    }

    public static String getMatkhau() {
        return matkhau;
    }

    public static void setMatkhau(String matkhau) {
        Taikhoan.matkhau = matkhau;
    }
    
    

}
