/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author lephucdienhung
 */
public class Giaingan {

    private int maHS;
    private String Ghichu;
    private String Loaichungtu;
    private int sotiengiaingan;
    private int sotienchuagiaingan;
    private String ngaygiaingan;

    public Giaingan(int maHS, String Ghichu, String Loaichungtu, int sotiengiaingan, int sotienchuagiaingan,
            String ngaygiaingan) {
        this.maHS = maHS;
        this.Ghichu = Ghichu;
        this.Loaichungtu = Loaichungtu;
        this.sotiengiaingan = sotiengiaingan;
        this.sotienchuagiaingan = sotienchuagiaingan;
        this.ngaygiaingan = ngaygiaingan;
    }

    public Giaingan() {
        this.setGhichu("");
        this.setLoaichungtu("");
        this.setMaHS(0);
        this.setNgaygiaingan("");
        this.setSotienchuagiaingan(0);
        this.setSotiengiaingan(0);
    }

    public int getMaHS() {
        return maHS;
    }

    public void setMaHS(int maHS) {
        this.maHS = maHS;
    }

    public String getGhichu() {
        return Ghichu;
    }

    public void setGhichu(String Ghichu) {
        this.Ghichu = Ghichu;
    }

    public String getLoaichungtu() {
        return Loaichungtu;
    }

    public void setLoaichungtu(String Loaichungtu) {
        this.Loaichungtu = Loaichungtu;
    }

    public int getSotiengiaingan() {
        return sotiengiaingan;
    }

    public void setSotiengiaingan(int sotiengiaingan) {
        this.sotiengiaingan = sotiengiaingan;
    }

    public int getSotienchuagiaingan() {
        return sotienchuagiaingan;
    }

    public void setSotienchuagiaingan(int sotienchuagiaingan) {
        this.sotienchuagiaingan = sotienchuagiaingan;
    }

    public String getNgaygiaingan() {
        return ngaygiaingan;
    }

    public void setNgaygiaingan(String ngaygiaingan) {
        this.ngaygiaingan = ngaygiaingan;
    }

}
