/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author lephucdienhung
 */
public class Tinhkhoanvay {

    int tiengocconlai;
    int goc;
    int lai;

    public Tinhkhoanvay(int tiengocconlai, int goc, int lai) {
        this.tiengocconlai = tiengocconlai;
        this.goc = goc;
        this.lai = lai;
    }

    public Tinhkhoanvay() {
    }

    public double getTiengocconlai() {
        return tiengocconlai;
    }

    public void setTiengocconlai(int tiengocconlai) {
        this.tiengocconlai = tiengocconlai;
    }

    public double getGoc() {
        return goc;
    }

    public void setGoc(int goc) {
        this.goc = goc;
    }

    public double getLai() {
        return lai;
    }

    public void setLai(int lai) {
        this.lai = lai;
    }

}
