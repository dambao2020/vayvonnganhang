package model;

public class Hosovayvon {

    private int Makh;
    private int Mahs;
    private int Manv;
    private int Sotienvay;
    private int Hanvay;
    private float LaiSuat;
    private int Mucdichvay;
    private int Hinhthucvay;
    private int Laidinhky;
    private int Gocdinhky;

    private String Ngaygiaingan;
    private String Mavung;
    private String Thuake;
    private String Ghichu;

    public Hosovayvon(int Makh, int Mahs, int Manv, String Mavung, int 
            Sotienvay, String Ngaygiaingan, int Hanvay,
            float LaiSuat, int Mucdichvay, int Hinhthucvay, int
                    Laidinhky, int Gocdinhky, String Thuake, String Ghichu) {
        this.Makh = Makh;
        this.Mahs = Mahs;
        this.Manv = Manv;
        this.Mavung = Mavung;
        this.Sotienvay = Sotienvay;
        this.Ngaygiaingan = Ngaygiaingan;
        this.Hanvay = Hanvay;
        this.LaiSuat = LaiSuat;
        this.Mucdichvay = Mucdichvay;
        this.Hinhthucvay = Hinhthucvay;
        this.Laidinhky = Laidinhky;
        this.Gocdinhky = Gocdinhky;
        this.Thuake = Thuake;
        this.Ghichu = Ghichu;
    }

    public Hosovayvon() {
        this.Makh = 0;
        this.Mahs = 0;
        this.Manv = 0;
        this.Mavung = "";
        this.Sotienvay = 0;
        this.Ngaygiaingan = "";
        this.Hanvay = 0;
        this.LaiSuat = 0;
        this.Mucdichvay = 0;
        this.Hinhthucvay = 0;
        this.Laidinhky = 0;
        this.Gocdinhky = 0;
        this.Thuake = "";
        this.Ghichu = "";
    }

    public int getMakh() {
        return Makh;
    }

    public void setMakh(int Makh) {
        this.Makh = Makh;
    }

    public int getMahs() {
        return Mahs;
    }

    public void setMahs(int Mahs) {
        this.Mahs = Mahs;
    }

    public int getManv() {
        return Manv;
    }

    public void setManv(int Manv) {
        this.Manv = Manv;
    }

    public String getMavung() {
        return Mavung;
    }

    public void setMavung(String Mavung) {
        this.Mavung = Mavung;
    }

    public int getSotienvay() {
        return Sotienvay;
    }

    public void setSotienvay(int Sotienvay) {
        this.Sotienvay = Sotienvay;
    }

    public String getNgaygiaingan() {
        return Ngaygiaingan;
    }

    public void setNgaygiaingan(String Ngaygiaingan) {
        this.Ngaygiaingan = Ngaygiaingan;
    }

    public int getHanvay() {
        return Hanvay;
    }

    public void setHanvay(int Hanvay) {
        this.Hanvay = Hanvay;
    }

    public float getLaiSuat() {
        return LaiSuat;
    }

    public void setLaiSuat(float LaiSuat) {
        this.LaiSuat = LaiSuat;
    }

    public int getMucdichvay() {
        return Mucdichvay;
    }

    public void setMucdichvay(int Mucdichvay) {
        this.Mucdichvay = Mucdichvay;
    }

    public int getHinhthucvay() {
        return Hinhthucvay;
    }

    public void setHinhthucvay(int Hinhthucvay) {
        this.Hinhthucvay = Hinhthucvay;
    }

    public int getLaidinhky() {
        return Laidinhky;
    }

    public void setLaidinhky(int Laidinhky) {
        this.Laidinhky = Laidinhky;
    }

    public int getGocdinhky() {
        return Gocdinhky;
    }

    public void setGocdinhky(int Gocdinhy) {
        this.Gocdinhky = Gocdinhy;
    }

    public String getThuake() {
        return Thuake;
    }

    public void setThuake(String Thuake) {
        this.Thuake = Thuake;
    }

    public String getGhichu() {
        return Ghichu;
    }

    public void setGhichu(String Ghichu) {
        this.Ghichu = Ghichu;
    }

}
